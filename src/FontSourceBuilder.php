<?php

namespace KDA\Laravel\Layouts\Fonts;

use Closure;
use KDA\Laravel\Layouts\Fonts\Traits\EvaluatesClosure;

class FontSourceBuilder
{
    use EvaluatesClosure;
    protected Closure | string $url ;
    protected $format;

    public static function make():static
    {
        return new static();
    }

    public function url(string | Closure $url):static
    {
        $this->url = $url;
        return $this;
    }

    public function format(string $format):static
    {
        $this->format = $format;
        return $this;
    }

    public function getUrl(){
        return $this->evaluate($this->url,$this->getEvaluationParameters());
    }

    public function getFormat(){
        return $this->format;
    }
}