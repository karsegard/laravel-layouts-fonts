<?php

namespace KDA\Laravel\Layouts\Fonts\Facades;

use Illuminate\Support\Facades\Facade;

class FontManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
