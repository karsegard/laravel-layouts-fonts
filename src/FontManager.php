<?php
namespace KDA\Laravel\Layouts\Fonts;

use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Layouts\Facades\LayoutManager;

//use Illuminate\Support\Facades\Blade;
class FontManager 
{
    protected $fonts = [];

    public function registerFontFace(string $family, array $files,$attributes=['font-weight'=>'normal','font-style'=>'normal']){
        $this->fonts[$family] = [
            'src'=>$files,
            'attributes'=>$attributes
        ];
    }

    public function renderFontFace(){
        $fonts = collect([]);
        foreach($this->fonts as $family=>$font){
            $src = collect($font['src'])->map(fn($file)=>"url(\"{$file->getUrl()}\") format(\"{$file->getFormat()}\")")->join(",\n\t");
            $attributes = collect($font['attributes'])->map(fn($attr,$key)=>"{$key}:{$attr}")->join(";\n");

            $fonts -> push("@font-face {\nfont-family:\"{$family}\";\nsrc: {$src};\n{$attributes}}");
        }
        return $fonts->join ("\n");
    }

    public function renderHook(){
        LayoutManager::registerRenderHook(
            'head.styles',
            function ($context) {
               return Blade::render('<style>'.$this->renderFontFace().'</style>');
            }
        );
    }
}