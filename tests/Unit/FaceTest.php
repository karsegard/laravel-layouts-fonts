<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\Layouts\Fonts\Facades\FontManager;
use KDA\Laravel\Layouts\Fonts\FontSourceBuilder;
use KDA\Tests\TestCase;

class FaceTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function test_1()
  {

    FontManager::registerFontFace('arial', [
      FontSourceBuilder::make()->url('aria.woff')->format('woff')
    ]);

    $result = FontManager::renderFontFace();

    $expected = "@font-face {
font-family:\"arial\";
src: url(\"aria.woff\") format(\"woff\");
font-weight:normal;
font-style:normal}";

    $this->assertEquals($result, $expected);
  }



  /** @test */
  function test_2()
  {

    FontManager::registerFontFace('arial', [
      FontSourceBuilder::make()->url('aria.woff')->format('woff'),
      FontSourceBuilder::make()->url('aria.woff2')->format('woff2')
    ]);

    $result = FontManager::renderFontFace();

    $expected = "@font-face {
font-family:\"arial\";
src: url(\"aria.woff\") format(\"woff\"),
\turl(\"aria.woff2\") format(\"woff2\");
font-weight:normal;
font-style:normal}";

    $this->assertEquals($result, $expected);
  }
}
